import torch
from custom_stable_diffusion_pipeline import CustomStableDiffusionPipeline
from typing import Any, Callable, Dict, List, Optional, Union
import diffusers
from diffusers import AltDiffusionPipeline, AltDiffusionImg2ImgPipeline, VersatileDiffusionPipeline, VersatileDiffusionDualGuidedPipeline, StableDiffusionUpscalePipeline
import PIL

class CustomAltDiffusionPipeline(AltDiffusionPipeline):
    @torch.no_grad()
    def img2img(
        self,
        prompt: Union[str, List[str]],
        image: Union[torch.FloatTensor, PIL.Image.Image],
        strength: float = 0.8,
        num_inference_steps: Optional[int] = 50,
        guidance_scale: Optional[float] = 7.5,
        negative_prompt: Optional[Union[str, List[str]]] = None,
        num_images_per_prompt: Optional[int] = 1,
        eta: Optional[float] = 0.0,
        generator: Optional[torch.Generator] = None,
        output_type: Optional[str] = "pil",
        return_dict: bool = True,
        callback: Optional[Callable[[int, int, torch.FloatTensor], None]] = None,
        callback_steps: Optional[int] = 1,
        height: int = 512,
        width: int = 512,
        **kwargs
    ):
        components = {"feature_extractor":None, "safety_checker":None}
        components.update(self.components)
        return AltDiffusionImg2ImgPipeline(**components)(
            prompt=prompt,
            image=image,
            strength=strength,
            num_inference_steps=num_inference_steps,
            guidance_scale=guidance_scale,
            negative_prompt=negative_prompt,
            num_images_per_prompt=num_images_per_prompt,
            eta=eta,
            generator=generator,
            output_type=output_type,
            return_dict=return_dict,
            callback=callback,
            callback_steps=callback_steps,
            **kwargs
        )

class CustomVersatileDiffusionPipeline(VersatileDiffusionPipeline):
    def enable_xformers_memory_efficient_attention(self):
        r"""
        Enable memory efficient attention as implemented in xformers.
        When this option is enabled, you should observe lower GPU memory usage and a potential speed up at inference
        time. Speed up at training time is not guaranteed.
        Warning: When Memory Efficient Attention and Sliced attention are both enabled, the Memory Efficient Attention
        is used.
        """
        self.set_use_memory_efficient_attention_xformers(True)

    def disable_xformers_memory_efficient_attention(self):
        r"""
        Disable memory efficient attention as implemented in xformers.
        """
        self.set_use_memory_efficient_attention_xformers(False)

def build_pipeline(model_id, model_type="sd", revision=None, torch_dtype=torch.float16, use_auth_token=None, vae=None, safety_checker=None, feature_extractor=None):
    kwargs = {
        "revision":revision,
        "safety_checker": safety_checker,
        "feature_extractor": feature_extractor,
        "torch_dtype":torch_dtype,
        "use_auth_token":use_auth_token,
        "vae":vae
    }
    if model_type == "alt":
        return CustomAltDiffusionPipeline.from_pretrained(model_id, **kwargs)
    if model_type == "versatile":
        return CustomVersatileDiffusionPipeline.from_pretrained(model_id, **kwargs)
    if model_type == "upscale":
        return StableDiffusionUpscalePipeline.from_pretrained(model_id, revision="fp16", torch_dtype=torch.float16)

    return CustomStableDiffusionPipeline.from_pretrained(model_id, **kwargs)

