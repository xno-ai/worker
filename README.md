# xno.ai worker

## Getting started

To run the worker locally see [https://www.xno.ai/faq-add-a-worker](https://www.xno.ai/faq-add-a-worker).

## Minimum requirements

You must have a gpu with 8GB VRAM or better to run stable-diffusion fast enough for xno.ai.

## Install

### Installing the conda environment

A suitable <a href="https://conda.io/">conda</a> environment named `xnoai` can be created
and activated with:

```bash
conda env create -f environment.yaml
conda activate xnoai
```

You can also update an existing environment by running:

```bash
conda install pytorch torchvision -c pytorch
pip install -r requirements.txt
```

### Install xformers

Required, greatly speeds up inference speed.

```
pip install ninja
pip install -U git+https://github.com/facebookresearch/xformers.git@main#egg=xformers
```

Installing will take a while.

## Setup

Once you install, in order to run the worker:

```bash
export HF_AUTH_TOKEN=...
export XNOAI_API_KEY=...
CUDA_VISIBLE_DEVICES=0 python worker.py
```

For windows, use `set` instead of `export`

## Features

### default enabled

| Feature | Requirements | To disable
| ------- | ------------ | ----------
| switch_models | You must also have 50 GB of free disk space to support model switching. | XNOAI_DISABLE_MODEL_SWITCHING=true
| inpainting | Downloads source images into memory and requires a model switch. | XNOAI_DISABLE_INPAINTING=true
| img2img | Downloads source images into memory. | XNOAI_DISABLE_IMG2IMG=true
| attention_slicing | Reduce memory requirements at the cost of inference speed. | XNOAI_DISABLE_ATTENTION_SLICING=true

### default disabled

| Feature | Requirements | To enable
| ------- | ------------ | ----------
| high_resolution | Increasing the image size beyond 512x512. Takes much larger amounts of vram | XNOAI_ENABLE_HIGH_RESOLUTION=true
| self_attention_guidance | Refine uses 2x as many unet passes and takes twice as long. | XNOAI_ENABLE_SELF_ATTENTION_GUIDANCE=true
| large_models1 | Support larger models up to 12GB | XNOAI_LARGE_MODELS1=true

### Recommended settings

## High end hardware

```
export XNOAI_ENABLE_EVERYTHING=true
```

## Resources

* <a href='xno.ai/faq-add-a-worker'>xno.ai worker faq</a>


